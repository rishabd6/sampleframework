Pod::Spec.new do |spec|

  spec.name         = 'SampleFramework_444'
  spec.version      = '1.0.0-beta11'
  spec.summary      = 'This is blend iOS SDK'

  spec.description  = 'this is a long description of blend iOS sdk.'

  spec.homepage     = 'https://gitlab.com/rishabd6/sampleframework'
  spec.license      = { :type => 'Apache', :file => 'LICENSE.txt' }

  spec.author       = { "Rishab Dutta" => "rishabd6@gmail.com" }

  spec.platform     = :ios, '10.0'
  spec.swift_versions = '4.0'

  spec.source       = { :git => 'https://gitlab.com/rishabd6/sampleframework.git', :tag => '1.0.0-beta11' }
  spec.vendored_frameworks = 'SampleFramework.xcframework'
#   spec.static_framework = true
  spec.pod_target_xcconfig = { 'VALID_ARCHS' => 'armv7 arm64 x86_64' }
      
  spec.dependency 'Firebase/Analytics'
  spec.dependency 'Firebase/RemoteConfig'
  spec.dependency 'Firebase/Crashlytics'

#   spec.dependency 'Google-Mobile-Ads-SDK'

  spec.dependency 'AmazonPublisherServicesSDK', '4.1.1'
  spec.dependency 'AmazonPublisherServicesAdMobAdapter', '2.0.1'

  spec.dependency 'GoogleMobileAdsMediationFacebook'

  spec.dependency 'GoogleMobileAdsMediationFyber'

  spec.dependency 'GoogleMobileAdsMediationMoPub'

  spec.dependency 'GoogleMobileAdsMediationInMobi'


end
